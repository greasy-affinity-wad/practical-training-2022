package org.example.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.annotation.MessageController;
import org.example.pojo.User;
import org.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
@MessageController
@Tag(name = "用户接口", description = "资源服务器，更新和提供和修改客户状态")
public class UserController {
    @Autowired
    UserService userService;

    @Operation(description = "查询函数，获取所有用户的状态（仅用户名和权限）", summary = "查询函数")
    @GetMapping("/getall")
    public List<User> getAllUser(){
        return userService.findAllUser();
    }
    @Operation(description = "注册新函数，权限只能是admin或user，不能是以存在的user", summary = "注册函数")
    @PostMapping("/signin")
    public void signIn(@RequestBody User user) throws Exception {
        userService.saveNewUser(user);
    }
    @Operation(description = "修改用户信息，需要opword必须为之前的密码，不能修改uname，用户必须已存在", summary = "修改信息")
    @PostMapping("/changeinfo")
    public void changeInfo(@RequestBody User user) throws Exception{
        userService.changeUserInfo(user);
    }
    @Operation(description = "修改用户信息，需要opword必须为之前的密码，用户必须已存在", summary = "注销用户")
    @PostMapping("/signout")
    public void signOut(@RequestBody User user) throws Exception{
        userService.deleteUser(user);
    }
}
