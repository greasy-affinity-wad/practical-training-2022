package org.example.repository;

import org.example.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUname(String uname);
}
