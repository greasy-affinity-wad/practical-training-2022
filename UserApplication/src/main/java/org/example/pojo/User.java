package org.example.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long Id;
    @Column(unique = true, nullable = false, length = 64)
    String uname;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(nullable = false, length = 64)
    String pword;
    @Column(nullable = false, length = 64)
    String role;

    @Transient
//    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    String opword;
}
