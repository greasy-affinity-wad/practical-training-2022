package org.example.service;

import org.example.pojo.Log;
import org.example.pojo.Logs;
import org.example.utils.Message;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "logs")
public interface LogService {
    @PostMapping("/save")
    public Message<Object> saveLog(@RequestBody Log log);
}
