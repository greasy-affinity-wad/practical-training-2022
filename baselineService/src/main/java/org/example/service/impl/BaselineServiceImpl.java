package org.example.service.impl;

import org.example.pojo.Baseline;
import org.example.mapper.BaselineMapper;
import org.example.pojo.Checks;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基线表(Baseline)表服务实现类
 *
 * @author JSY2019
 * @since 2022-06-20 17:11:52
 * @version 1.0
 */
@Service("baselineService")
public class BaselineServiceImpl {
    @Resource
    private BaselineMapper baselineMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    public Map<String, Object> selectCount(String name) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.baselineMapper.selectCount(name));
        return map;
    }

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    public Map<String, Object> selectAll() {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.baselineMapper.selectAll());
        return map;
    }

    /**
     * 通过ID查询数据
     *
     * @param mac MAC地址
     * @return 实例对象
     */
    public List<Baseline> selectById(String mac) {
        return this.baselineMapper.selectById(mac);
    }
    /**
     * 通过ID查询单条数据
     *
     * @param mac MAC地址
     * @return 实例对象
     */
    public List<Baseline> selectOneById(String mac) {
        return this.baselineMapper.selectOneById(mac);
    }
    /**
     * 通过时间查询数据
     *
     * @param mac MAC地址
     * @return 实例对象
     */
    public List<Baseline> selectOneByTime(String mac, Timestamp leftrange, Timestamp rightrange) {
        return this.baselineMapper.selectOneByTime(mac,leftrange,rightrange);
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @param name 查询条数
     * @return 对象列表
     */
    public Map<String, Object> selectByPage(int page, String name) {
    // 获取当前表中的总记录
        int tableCount = this.baselineMapper.selectCount(name);
        // 总页码计算   (总条数 - 1) / 每页显示条数  + 1
        // (100 - 1) / 10 + 1 = 10        (101 - 1) / 10 + 1 = 11      (99 - 1) / 10 + 1 = 10
        int pageCount = (tableCount - 1) / 10 + 1;
        // 计算每页开始的下标值
        int index = (page - 1) * 10;
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "查询成功");
        map.put("pageCount", pageCount);  // 查询的记录总页码
        map.put("count", tableCount);     // 当前表中的总条数
        map.put("data", this.baselineMapper.selectByPage(index, name));
        return map;
    }

    /**
     * 新增数据
     *
     * @param baseline 实例对象
     * @return 实例对象
     */
    public Map<String, Object> insert(Baseline baseline) {
        // UUID.randomUUID()  返回内容：asd21321-ewrewrew213213-123213zsad-123asdasd这样的形态
        this.baselineMapper.insert(baseline);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "新增成功");
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param baseline 实例对象
     * @return 实例对象
     */
    public Map<String, Object> updateById(Baseline baseline) {
        this.baselineMapper.updateById(baseline);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "更新成功");
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    public Map<String, Object> deleteById(String id) {
        this.baselineMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "删除成功");
        return map;
    }

    /**
     * 批量增加数据
     *
     * @param lic 增加数据的list
     * @return 是否成功
     */
    public Map<String, Object> multiinsert(List<Baseline> lic) {
        // UUID.randomUUID()  返回内容：asd21321-ewrewrew213213-123213zsad-123asdasd这样的形态
        for (Baseline baseline : lic) {
            this.baselineMapper.insert(baseline);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "新增成功");
        return map;
    }

}