package org.example.service.impl;

import org.example.pojo.Baseline;
import org.example.pojo.Basics;
import org.example.mapper.BasicsMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 基础信息表(Basics)表服务实现类
 *
 * @author JSY2019
 * @since 2022-06-20 11:16:32
 * @version 1.0
 */
@Service("basicsService")
public class BasicsServiceImpl {
    @Resource
    private BasicsMapper basicsMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    public Map<String, Object> selectCount(String name) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.basicsMapper.selectCount(name));
        return map;
    }

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    public Map<String, Object> selectAll() {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.basicsMapper.selectAll());
        return map;
    }

    /**
     * 通过ID查询数据
     *
     * @param uname 主键
     * @return 实例对象
     */
    public List<Basics> selectById(String uname) {
        return this.basicsMapper.selectById(uname);
    }
    /**
     * 通过ID查询单条数据
     *
     * @param uname 主键
     * @return 实例对象
     */
    public Basics selectOneById(String uname){
        return this.basicsMapper.selectOneById(uname);
    }
    /**
     * 通过时间查询数据
     *
     * @param uname 主键
     * @return 实例对象
     */
    public List<Basics> selectOneByTime(String uname, Timestamp leftrange, Timestamp rightrange){
        return this.basicsMapper.selectOneByTime(uname,leftrange,rightrange);
    }




    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @param name 查询条数
     * @return 对象列表
     */
    public Map<String, Object> selectByPage(int page, String name) {
    // 获取当前表中的总记录
        int tableCount = this.basicsMapper.selectCount(name);
        // 总页码计算   (总条数 - 1) / 每页显示条数  + 1
        // (100 - 1) / 10 + 1 = 10        (101 - 1) / 10 + 1 = 11      (99 - 1) / 10 + 1 = 10
        int pageCount = (tableCount - 1) / 10 + 1;
        // 计算每页开始的下标值
        int index = (page - 1) * 10;
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "查询成功");
        map.put("pageCount", pageCount);  // 查询的记录总页码
        map.put("count", tableCount);     // 当前表中的总条数
        map.put("data", this.basicsMapper.selectByPage(index, name));
        return map;
    }

    /**
     * 新增数据
     *
     * @param basics 实例对象
     * @return 实例对象
     */
    public Map<String, Object> insert(Basics basics) {
        // UUID.randomUUID()  返回内容：asd21321-ewrewrew213213-123213zsad-123asdasd这样的形态
        this.basicsMapper.insert(basics);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "新增成功");
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param basics 实例对象
     * @return 实例对象
     */
    public Map<String, Object> updateById(Basics basics) {
        this.basicsMapper.updateById(basics);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "更新成功");
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param uname 主键
     * @return 是否成功
     */
    public Map<String, Object> deleteById(String uname) {
        this.basicsMapper.deleteById(uname);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "删除成功");
        return map;
    }
}