package org.example.service.impl;

import org.example.pojo.Hardware;
import org.example.mapper.HardwareMapper;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 包括硬件信息、mac信息在内的通用信息(Hardware)表服务实现类
 *
 * @author JSY2019
 * @since 2022-06-20 14:30:19
 * @version 1.0
 */
@Service("hardwareService")
public class HardwareServiceImpl {
    @Resource
    private HardwareMapper hardwareMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    public Map<String, Object> selectCount(String name) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.hardwareMapper.selectCount(name));
        return map;
    }

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    public Map<String, Object> selectAll() {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.hardwareMapper.selectAll());
        return map;
    }

    /**
     * 通过ID查询数据
     *
     * @param mac mac地址
     * @return 实例对象
     */
    public List<Hardware> selectById(String mac) {
        return this.hardwareMapper.selectById(mac);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param mac mac地址
     * @return 实例对象
     */
    public Hardware selectOneById(String mac) {
        return this.hardwareMapper.selectOneById(mac);
    }

    /**
     * 通过时间查询单条数据
     *
     * @param mac mac地址
     * @return 实例对象
     */
    public List<Hardware> selectOneByTime(String mac, Timestamp leftrange, Timestamp rightrange) {
        return this.hardwareMapper.selectOneByTime(mac,leftrange,rightrange);
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @param name 查询条数
     * @return 对象列表
     */
    public Map<String, Object> selectByPage(int page, String name) {
    // 获取当前表中的总记录
        int tableCount = this.hardwareMapper.selectCount(name);
        // 总页码计算   (总条数 - 1) / 每页显示条数  + 1
        // (100 - 1) / 10 + 1 = 10        (101 - 1) / 10 + 1 = 11      (99 - 1) / 10 + 1 = 10
        int pageCount = (tableCount - 1) / 10 + 1;
        // 计算每页开始的下标值
        int index = (page - 1) * 10;
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "查询成功");
        map.put("pageCount", pageCount);  // 查询的记录总页码
        map.put("count", tableCount);     // 当前表中的总条数
        map.put("data", this.hardwareMapper.selectByPage(index, name));
        return map;
    }

    /**
     * 新增数据
     *
     * @param hardware 实例对象
     * @return 实例对象
     */
    public Map<String, Object> insert(Hardware hardware) {
        // UUID.randomUUID()  返回内容：asd21321-ewrewrew213213-123213zsad-123asdasd这样的形态
        try{
            this.hardwareMapper.insert(hardware);
        }catch (Exception e){
            System.out.println(e);
        }

        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "新增成功");
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param hardware 实例对象
     * @return 实例对象
     */
    public Map<String, Object> updateById(Hardware hardware) {
        this.hardwareMapper.updateById(hardware);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "更新成功");
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    public Map<String, Object> deleteById(String id) {
        this.hardwareMapper.deleteById(id);
        Map<String, Object> map = new HashMap<>();
        map.put("code", 200);   // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("msg", "删除成功");
        return map;
    }
}