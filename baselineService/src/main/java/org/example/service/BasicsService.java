package org.example.service;

import org.example.pojo.Basics;

import java.util.Map;

/**
 * 基础信息表(Basics)表服务接口类
 *
 * @author JSY2019
 * @since 2022-06-20 11:16:30
 * @version 1.0
 */
public interface BasicsService {
    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    Map<String, Object> chaXunCount(String mingCheng);

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    Map<String, Object> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param uname 主键
     * @return 实例对象
     */
    Map<String, Object> chaXunById(String uname);

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @param mingCheng 查询条数
     * @return 对象列表
     */
    Map<String, Object> chaXunFenYe(int page, String mingCheng);

    /**
     * 新增数据
     *
     * @param basics 实例对象
     * @return 实例对象
     */
    Map<String, Object> xinZeng(Basics basics);

    /**
     * 通过ID查询单条数据
     *
     * @param basics 实例对象
     * @return 实例对象
     */
    Map<String, Object> gengXinById(Basics basics);

    /**
     * 通过主键删除数据
     *
     * @param uname 主键
     * @return 是否成功
     */
    Map<String, Object> shanChuById(String uname);
}