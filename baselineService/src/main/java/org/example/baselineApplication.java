package org.example;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.openfeign.EnableFeignClients;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@MapperScan("org.example.mapper")
//@EnableResourceServer
//@EnableFeignClients
@EnableSwagger2
public class baselineApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(baselineApplication.class, args);
    }
}