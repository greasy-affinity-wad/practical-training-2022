package org.example.pojo;

import java.util.List;
import java.util.Map;

public class SystemServiceReceive {
    Map<String,Object> log;
    List<ServiceReceive> service;

    public Map<String, Object> getLog() {
        return log;
    }

    public void setLog(Map<String, Object> log) {
        this.log = log;
    }

    public List<ServiceReceive> getService() {
        return service;
    }

    public void setService(List<ServiceReceive> service) {
        this.service = service;
    }
}



