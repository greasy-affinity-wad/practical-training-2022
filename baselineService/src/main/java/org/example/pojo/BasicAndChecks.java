package org.example.pojo;

import java.util.List;
import java.util.Map;


public class BasicAndChecks {
    Map<String,Object> log;
    Map<String,Object> basic;
//    List<Baseline> baselines;
    List<Checks> checks;

    public Map<String, Object> getBasic() {
        return basic;
    }

    public void setBasic(Map<String, Object> basic) {
        this.basic = basic;
    }

//    public List<Baseline> getBaselines() {
//        return baselines;
//    }
//
//    public void setBaselines(List<Baseline> check) {
//        this.baselines = baselines;
//    }

    public Map<String, Object> getLog() {
        return log;
    }

    public void setLog(Map<String, Object> log) {
        this.log = log;
    }

    public List<Checks> getChecks() {
        return checks;
    }

    public void setChecks(List<Checks> checks) {
        this.checks = checks;
    }
}
