package org.example.pojo;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 基线表(Baseline)实体类
 *
 * @author JSY2019
 * @since 2022-06-20 17:11:52
 */
@ApiModel(value = "Baseline",description = "基线表")
public class Baseline implements Serializable {
    private static final long serialVersionUID = 144949907596986037L;
    /**
    * id
    */
	@ApiModelProperty(name = "id",notes = "id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 用户mac地址
    */
	@ApiModelProperty(name = "mac",notes = "用户mac地址",dataType = "String",required = true)
    private String mac;
    /**
    * 更新时间
    */
	@ApiModelProperty(name = "updateTime",notes = "更新时间",dataType = "Date",required = true)
    private Date updateTime;
    /**
    * 基线名
    */
	@ApiModelProperty(name = "key",notes = "基线名",dataType = "String",required = true)
    private String key;
    /**
    * 是否已经核查
    */
	@ApiModelProperty(name = "evalued",notes = "是否已经核查",dataType = "String",required = true)
    private String evalued;
    /**
    * 是否符合核查结果
    */
	@ApiModelProperty(name = "result",notes = "是否符合核查结果",dataType = "String",required = true)
    private String result;
    /**
    * 系统的真实值
    */
	@ApiModelProperty(name = "relVal",notes = "系统的真实值",dataType = "String",required = true)
    private String relVal;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
        
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
        
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
        
    public String getEvalued() {
        return evalued;
    }

    public void setEvalued(String evalued) {
        this.evalued = evalued;
    }
        
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
        
    public String getRelVal() {
        return relVal;
    }

    public void setRelVal(String relVal) {
        this.relVal = relVal;
    }

}