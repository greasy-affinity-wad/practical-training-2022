package org.example.pojo;

public class Checks {
        private String key;
        private String evalued;
        private String result;
        private String rel_val;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getEvalued() {
            return evalued;
        }

        public void setEvalued(String evalued) {
            this.evalued = evalued;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getRel_val() {
            return rel_val;
        }

        public void setRel_val(String rel_val) {
            this.rel_val = rel_val;
        }
}
