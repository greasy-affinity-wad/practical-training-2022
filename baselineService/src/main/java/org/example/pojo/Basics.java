package org.example.pojo;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 基础信息表(Basics)实体类
 *
 * @author JSY2019
 * @since 2022-06-20 11:16:33
 */
@ApiModel(value = "Basics",description = "基础信息表")
public class Basics implements Serializable {
    private static final long serialVersionUID = 171363683146685818L;
    /**
    * MAC地址
    */
	@ApiModelProperty(name = "uname",notes = "MAC地址",dataType = "String",required = true)
    private String uname;
    /**
    * 时间戳
    */
	@ApiModelProperty(name = "timestamp",notes = "时间戳",dataType = "Date",required = true)
    private Date timestamp;
    /**
    * 平台
    */
	@ApiModelProperty(name = "platform",notes = "平台",dataType = "String",required = true)
    private String platform;
    /**
    * 版本
    */
	@ApiModelProperty(name = "version",notes = "版本",dataType = "String",required = true)
    private String version;
    /**
    * 架构
    */
	@ApiModelProperty(name = "architecture",notes = "架构",dataType = "String",required = true)
    private String architecture;
    /**
    * machine
    */
	@ApiModelProperty(name = "machine",notes = "machine",dataType = "String",required = true)
    private String machine;
    /**
    * node
    */
	@ApiModelProperty(name = "node",notes = "node",dataType = "String",required = true)
    private String node;
    /**
    * 处理器
    */
	@ApiModelProperty(name = "processor",notes = "处理器",dataType = "String",required = true)
    private String processor;
    /**
    * 系统
    */
	@ApiModelProperty(name = "system",notes = "系统",dataType = "String",required = true)
    private String system;

        
    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }
        
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
        
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }
        
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
        
    public String getArchitecture() {
        return architecture;
    }

    public void setArchitecture(String architecture) {
        this.architecture = architecture;
    }
        
    public String getMachine() {
        return machine;
    }

    public void setMachine(String machine) {
        this.machine = machine;
    }
        
    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }
        
    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }
        
    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

}