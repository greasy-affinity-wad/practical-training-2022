package org.example.pojo;

public class ServiceReceive{
    private String caption;
    private String descriptions;
    private String state;
    private String startmode;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStartmode() {
        return startmode;
    }

    public void setStartmode(String startmode) {
        this.startmode = startmode;
    }
}
