package org.example.pojo;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 网络信息(NetInfo)实体类
 *
 * @author JSY2019
 * @since 2022-06-20 20:54:57
 */
@ApiModel(value = "NetInfo",description = "网络信息")
public class NetInfo implements Serializable {
    private static final long serialVersionUID = 733183173876244010L;
    
	@ApiModelProperty(name = "id",notes = "${column.comment}",dataType = "Integer",required = true)
    private Integer id;
    /**
    * MAC地址
    */
	@ApiModelProperty(name = "mac",notes = "MAC地址",dataType = "String",required = true)
    private String mac;
    /**
    * 更新时间
    */
	@ApiModelProperty(name = "updateTime",notes = "更新时间",dataType = "Date",required = true)
    private Date updateTime;
    /**
    * 开放的端口
    */
	@ApiModelProperty(name = "openport",notes = "开放的端口",dataType = "String",required = true)
    private String openport;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
        
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
        
    public String getOpenport() {
        return openport;
    }

    public void setOpenport(String openport) {
        this.openport = openport;
    }

}