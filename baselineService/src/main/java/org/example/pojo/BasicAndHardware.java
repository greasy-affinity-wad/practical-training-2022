package org.example.pojo;
import java.util.Map;

public class BasicAndHardware {
    Map<String,Object> log;
    Map<String,Object> basic;
    Map<String,Object> hardware;

    public Map<String, Object> getBasic() {
        return basic;
    }

    public void setBasic(Map<String, Object> basic) {
        this.basic = basic;
    }

    public Map<String, Object> getLog() {
        return log;
    }

    public void setLog(Map<String, Object> log) {
        this.log = log;
    }

    public Map<String, Object> getHardware() {
        return hardware;
    }

    public void setHardware(Map<String, Object> hardware) {
        this.hardware = hardware;
    }
}

