package org.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Log {
    private String uname;
    private Timestamp timestamp;
    private String des;
    public Log(Logs logs){
        this.uname = logs.getUname();

        StringBuilder stb = new StringBuilder();
        for(String item : logs.getDes()){
            stb.append(item).append(",");
        }
        this.des = stb.toString().substring(0,stb.toString().length()-1);

        this.timestamp = Timestamp.valueOf(logs.getTimestamp());
    }
}
