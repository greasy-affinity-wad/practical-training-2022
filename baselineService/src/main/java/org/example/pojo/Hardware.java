package org.example.pojo;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 包括硬件信息、mac信息在内的通用信息(Hardware)实体类
 *
 * @author JSY2019
 * @since 2022-06-20 14:30:25
 */
@ApiModel(value = "Hardware",description = "包括硬件信息、mac信息在内的通用信息")
public class Hardware implements Serializable {
    private static final long serialVersionUID = -96444902150039113L;
    
	@ApiModelProperty(name = "id",notes = "${column.comment}",dataType = "Integer",required = true)
    private Integer id;
    /**
    * mac地址
    */
	@ApiModelProperty(name = "mac",notes = "mac地址",dataType = "String",required = true)
    private String mac;
    /**
    * 更新时间
    */
	@ApiModelProperty(name = "updateTime",notes = "更新时间",dataType = "Date",required = true)
    private Date updateTime;
    /**
    * 操作系统
    */
	@ApiModelProperty(name = "infoOs",notes = "操作系统",dataType = "String",required = true)
    private String infoOs;
    /**
    * 用户名
    */
	@ApiModelProperty(name = "infoFullname",notes = "用户名",dataType = "String",required = true)
    private String infoFullname;
    /**
    * 操作系统架构
    */
	@ApiModelProperty(name = "infoOsArchitecture",notes = "操作系统架构",dataType = "String",required = true)
    private String infoOsArchitecture;
    /**
    * 语言
    */
	@ApiModelProperty(name = "infoMuLanguages",notes = "语言",dataType = "String",required = true)
    private String infoMuLanguages;
    /**
    * 序列号
    */
	@ApiModelProperty(name = "infoSerialnumber",notes = "序列号",dataType = "String",required = true)
    private String infoSerialnumber;
    /**
    * CPU数
    */
	@ApiModelProperty(name = "infoCpuCount",notes = "CPU数",dataType = "Integer",required = true)
    private Integer infoCpuCount;
    /**
    * 主板信息
    */
	@ApiModelProperty(name = "infoMainboard",notes = "主板信息",dataType = "String",required = true)
    private String infoMainboard;
    /**
    * 主板model
    */
	@ApiModelProperty(name = "infoBoardModel",notes = "主板model",dataType = "String",required = true)
    private String infoBoardModel;
    /**
    * 系统种类
    */
	@ApiModelProperty(name = "infoSystemtype",notes = "系统种类",dataType = "String",required = true)
    private String infoSystemtype;
    /**
    * 物理内存大小
    */
	@ApiModelProperty(name = "infoPhysicalMemory",notes = "物理内存大小",dataType = "String",required = true)
    private String infoPhysicalMemory;
    /**
    * CPU名字
    */
	@ApiModelProperty(name = "infoCpuName",notes = "CPU名字",dataType = "String",required = true)
    private String infoCpuName;
    /**
    * 时钟速度
    */
	@ApiModelProperty(name = "infoClockSpeed",notes = "时钟速度",dataType = "Integer",required = true)
    private Integer infoClockSpeed;
    /**
    * 内核数
    */
	@ApiModelProperty(name = "infoNumberCore",notes = "内核数",dataType = "Integer",required = true)
    private Integer infoNumberCore;
    /**
    * data宽度
    */
	@ApiModelProperty(name = "infoDataWidth",notes = "data宽度",dataType = "Integer",required = true)
    private Integer infoDataWidth;
    /**
    * Socket设计
    */
	@ApiModelProperty(name = "infoSocketDesigination",notes = "Socket设计",dataType = "String",required = true)
    private String infoSocketDesigination;
    /**
    * L2缓存大小
    */
	@ApiModelProperty(name = "infoL2Cache",notes = "L2缓存大小",dataType = "Integer",required = true)
    private Integer infoL2Cache;
    /**
    * L3缓存大小
    */
	@ApiModelProperty(name = "infoL3Cache",notes = "L3缓存大小",dataType = "Integer",required = true)
    private Integer infoL3Cache;

    @ApiModelProperty(name = "memUsage",notes = "内存占用",dataType = "String",required = true)
    private String memUsage;

    @ApiModelProperty(name = "cpuUsage",notes = "CPU占用",dataType = "String",required = true)
    private String cpuUsage;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
        
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
        
    public String getInfoOs() {
        return infoOs;
    }

    public void setInfoOs(String infoOs) {
        this.infoOs = infoOs;
    }
        
    public String getInfoFullname() {
        return infoFullname;
    }

    public void setInfoFullname(String infoFullname) {
        this.infoFullname = infoFullname;
    }
        
    public String getInfoOsArchitecture() {
        return infoOsArchitecture;
    }

    public void setInfoOsArchitecture(String infoOsArchitecture) {
        this.infoOsArchitecture = infoOsArchitecture;
    }
        
    public String getInfoMuLanguages() {
        return infoMuLanguages;
    }

    public void setInfoMuLanguages(String infoMuLanguages) {
        this.infoMuLanguages = infoMuLanguages;
    }
        
    public String getInfoSerialnumber() {
        return infoSerialnumber;
    }

    public void setInfoSerialnumber(String infoSerialnumber) {
        this.infoSerialnumber = infoSerialnumber;
    }
        
    public Integer getInfoCpuCount() {
        return infoCpuCount;
    }

    public void setInfoCpuCount(Integer infoCpuCount) {
        this.infoCpuCount = infoCpuCount;
    }
        
    public String getInfoMainboard() {
        return infoMainboard;
    }

    public void setInfoMainboard(String infoMainboard) {
        this.infoMainboard = infoMainboard;
    }
        
    public String getInfoBoardModel() {
        return infoBoardModel;
    }

    public void setInfoBoardModel(String infoBoardModel) {
        this.infoBoardModel = infoBoardModel;
    }
        
    public String getInfoSystemtype() {
        return infoSystemtype;
    }

    public void setInfoSystemtype(String infoSystemtype) {
        this.infoSystemtype = infoSystemtype;
    }
        
    public String getInfoPhysicalMemory() {
        return infoPhysicalMemory;
    }

    public void setInfoPhysicalMemory(String infoPhysicalMemory) {
        this.infoPhysicalMemory = infoPhysicalMemory;
    }
        
    public String getInfoCpuName() {
        return infoCpuName;
    }

    public void setInfoCpuName(String infoCpuName) {
        this.infoCpuName = infoCpuName;
    }
        
    public Integer getInfoClockSpeed() {
        return infoClockSpeed;
    }

    public void setInfoClockSpeed(Integer infoClockSpeed) {
        this.infoClockSpeed = infoClockSpeed;
    }
        
    public Integer getInfoNumberCore() {
        return infoNumberCore;
    }

    public void setInfoNumberCore(Integer infoNumberCore) {
        this.infoNumberCore = infoNumberCore;
    }
        
    public Integer getInfoDataWidth() {
        return infoDataWidth;
    }

    public void setInfoDataWidth(Integer infoDataWidth) {
        this.infoDataWidth = infoDataWidth;
    }
        
    public String getInfoSocketDesigination() {
        return infoSocketDesigination;
    }

    public void setInfoSocketDesigination(String infoSocketDesigination) {
        this.infoSocketDesigination = infoSocketDesigination;
    }
        
    public Integer getInfoL2Cache() {
        return infoL2Cache;
    }

    public void setInfoL2Cache(Integer infoL2Cache) {
        this.infoL2Cache = infoL2Cache;
    }
        
    public Integer getInfoL3Cache() {
        return infoL3Cache;
    }

    public void setInfoL3Cache(Integer infoL3Cache) {
        this.infoL3Cache = infoL3Cache;
    }

    public String getMemUsage() {
        return memUsage;
    }

    public void setMemUsage(String memUsage) {
        this.memUsage = memUsage;
    }

    public String getCpuUsage() {
        return cpuUsage;
    }

    public void setCpuUsage(String cpuUsage) {
        this.cpuUsage = cpuUsage;
    }
}