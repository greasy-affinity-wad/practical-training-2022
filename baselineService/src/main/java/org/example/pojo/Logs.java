package org.example.pojo;

public class Logs {
    private String[] des;
    private String uname;
    private String timestamp;

    public String[] getDes() {
        return des;
    }

    public void setDes(String[] des) {
        this.des = des;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
