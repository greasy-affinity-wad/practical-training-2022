package org.example.pojo;

import java.util.List;
import java.util.Map;

public class NetInfoReceive {
    Map<String, Object> log;
    //NetInfo netInfo;
    List<String> port;

    public Map<String, Object> getLog() {
        return log;
    }

    public void setLog(Map<String, Object> log) {
        this.log = log;
    }

//    public NetInfo getNetInfo() {
//        return netInfo;
//    }
//
//    public void setNetInfo(NetInfo netInfo) {
//        this.netInfo = netInfo;
//    }


    public List<String> getPort() {
        return port;
    }

    public void setPort(List<String> port) {
        this.port = port;
    }
}
