package org.example.pojo;

import java.util.List;
import java.util.Map;

public class LinuxReceive {
    Map<String,Object> log;
    Map<String,Object> basic;
    Map<String,Object> hardware;
    List<String> port;
    List<Checks> baseline;
    List<ServiceReceive> service;

    public Map<String, Object> getLog() {
        return log;
    }

    public void setLog(Map<String, Object> log) {
        this.log = log;
    }

    public Map<String, Object> getBasic() {
        return basic;
    }

    public void setBasic(Map<String, Object> basic) {
        this.basic = basic;
    }

    public Map<String, Object> getHardware() {
        return hardware;
    }

    public void setHardware(Map<String, Object> hardware) {
        this.hardware = hardware;
    }

    public List<String> getPort() {
        return port;
    }

    public void setPort(List<String> port) {
        this.port = port;
    }

    public List<Checks> getBaseline() {
        return baseline;
    }

    public void setBaseline(List<Checks> baseline) {
        this.baseline = baseline;
    }

    public List<ServiceReceive> getService() {
        return service;
    }

    public void setService(List<ServiceReceive> service) {
        this.service = service;
    }
}
