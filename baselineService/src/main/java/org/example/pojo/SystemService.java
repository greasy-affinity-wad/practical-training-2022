package org.example.pojo;

import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 系统服务信息(SystemService)实体类
 *
 * @author JSY2019
 * @since 2022-06-20 20:15:14
 */
@ApiModel(value = "SystemService",description = "系统服务信息")
public class SystemService implements Serializable {
    private static final long serialVersionUID = -42094843606290423L;
    
	@ApiModelProperty(name = "id",notes = "${column.comment}",dataType = "Integer",required = true)
    private Integer id;
    /**
    * MAC
    */
	@ApiModelProperty(name = "mac",notes = "MAC",dataType = "String",required = true)
    private String mac;
    /**
    * 时间
    */
	@ApiModelProperty(name = "updateTime",notes = "时间",dataType = "Date",required = true)
    private Date updateTime;
    /**
    * 服务名
    */
	@ApiModelProperty(name = "caption",notes = "服务名",dataType = "String",required = true)
    private String caption;
    /**
    * 服务描述
    */
	@ApiModelProperty(name = "descriptions",notes = "服务描述",dataType = "String",required = true)
    private String descriptions;
    /**
    * 服务状态
    */
	@ApiModelProperty(name = "state",notes = "服务状态",dataType = "String",required = true)
    private String state;
    /**
    * 启动方式
    */
	@ApiModelProperty(name = "startmode",notes = "启动方式",dataType = "String",required = true)
    private String startmode;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }
        
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
        
    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
        
    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }
        
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
        
    public String getStartmode() {
        return startmode;
    }

    public void setStartmode(String startmode) {
        this.startmode = startmode;
    }

}