package org.example.mapper;

import org.example.pojo.SystemService;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

/**
 * 系统服务信息(SystemService)表数据库访问层
 *
 * @author JSY2019
 * @since 2022-06-20 20:15:14
 * @version 1.0
 */
public interface SystemServiceMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<SystemService> selectAll();

    /**
     * 通过ID查询数据
     *
     * @param mac MAC地址
     * @return 实例对象
     */
    List<SystemService> selectById(@Param("mac") String mac);

    /**
     * 通过ID查询一条数据
     *
     * @param mac MAC地址
     * @return 实例对象
     */
    List<SystemService> selectOneById(@Param("mac") String mac);

    /**
     * 通过时间查询数据
     *
     * @param mac MAC地址
     * @return 实例对象
     */
    List<SystemService> selectOneByTime(String mac, Timestamp leftrange, Timestamp rightrange);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int selectCount(String name);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param index    当前查询开始页中的第一个下标值
     * @param name  需要模糊查询的内容
     * @return 对象列表
     */
    List<SystemService> selectByPage(@Param("index") int index, @Param("name")String name);

    /**
     * 新增数据
     *
     * @param systemService 实例对象
     */
    void insert(SystemService systemService);

    /**
     * 修改数据
     *
     * @param systemService 实例对象
     * @return 影响行数
     */
    int updateById(SystemService systemService);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(@Param("id") String id);

}