package org.example.mapper;

import org.example.pojo.Hardware;
import org.apache.ibatis.annotations.Param;

import java.sql.Timestamp;
import java.util.List;

/**
 * 包括硬件信息、mac信息在内的通用信息(Hardware)表数据库访问层
 *
 * @author JSY2019
 * @since 2022-06-20 14:30:25
 * @version 1.0
 */
public interface HardwareMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<Hardware> selectAll();

    /**
     * 通过ID查询数据
     *
     * @param mac mac地址
     * @return 实例对象
     */
    List<Hardware> selectById(@Param("mac") String mac);

    /**
     * 通过ID查询单条数据
     *
     * @param mac mac地址
     * @return 实例对象
     */
    Hardware selectOneById(String mac);

    /**
     * 通过ID查询数据
     *
     * @param mac mac地址
     * @return 实例对象
     */
    List<Hardware> selectOneByTime(String mac, Timestamp leftrange, Timestamp rightrange);


    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int selectCount(String name);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param index    当前查询开始页中的第一个下标值
     * @param name  需要模糊查询的内容
     * @return 对象列表
     */
    List<Hardware> selectByPage(@Param("index") int index, @Param("name")String name);

    /**
     * 新增数据
     *
     * @param hardware 实例对象
     */
    void insert(Hardware hardware);

    /**
     * 修改数据
     *
     * @param hardware 实例对象
     * @return 影响行数
     */
    int updateById(Hardware hardware);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(@Param("id") String id);

}