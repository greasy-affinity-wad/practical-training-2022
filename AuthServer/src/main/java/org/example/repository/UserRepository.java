package org.example.repository;

import org.example.utils.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;
/*
@author 荆延龙
 */
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUname(String uname);
}
