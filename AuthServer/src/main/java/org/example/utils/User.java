package org.example.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;
/*
@author 荆延龙
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User {
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long Id;
    @Column(unique = true, nullable = false, length = 64)
    String uname;
    @Column(nullable = false, length = 64)
    String pword;
    @Column(nullable = false, length = 64)
    String role;
}