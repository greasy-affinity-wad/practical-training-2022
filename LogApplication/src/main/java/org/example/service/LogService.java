package org.example.service;

import org.example.pojo.Log;
import org.example.repository.LogRepository;
import org.example.utils.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.Timestamp;
import java.util.List;
/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
@Service
public class LogService {
    @Autowired
    LogRepository logRepository;
    public List<Log> findByUname(String uname){
        return logRepository.findByUname(uname);
    }
    public List<Log> findByTime(Timestamp t1, Timestamp t2) throws Exception {
        if (!t1.before(t2) || System.currentTimeMillis()<t1.getTime() || System.currentTimeMillis() < t2.getTime()) throw new Exception("请输入正确的时间格式！");
        return logRepository.findByTimestampBetween(t1,t2);
    }
    public List<Log> findByTimeBetweenAndUname(Timestamp t1, Timestamp t2, String uname) throws Exception{
        if (!t1.before(t2) || System.currentTimeMillis()<t1.getTime() || System.currentTimeMillis() < t2.getTime()) throw new Exception("请输入正确的时间格式！");
        return logRepository.findByTimestampBetweenAndUname(t1, t2, uname);
    }
    public Log findByTimeAndUname(Timestamp timestamp, String uname) throws Exception{
        Log log = logRepository.findByTimestampAndUname(timestamp, uname).orElse(null);
        if (log==null){
            throw new Exception("未找到相关记录");
        }
        return log;
    }
    public void saveLog(Log log){
        logRepository.saveAndFlush(log);
    }
    public List<Log> findAll(){
        return logRepository.findAll();
    }
}
