package org.example.repository;

import org.example.pojo.Log;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
public interface LogRepository extends JpaRepository<Log, Long>{
    List<Log> findByUname(String uname);
    List<Log> findByTimestampBetween(Timestamp timestamp1, Timestamp timestamp2);
    List<Log> findByTimestampBetweenAndUname(Timestamp timestamp1, Timestamp timestamp2, String uname);
    Optional<Log> findByTimestampAndUname(Timestamp timestamp, String uname);
}
