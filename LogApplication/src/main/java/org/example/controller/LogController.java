package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.example.annotation.MessageController;
import org.example.pojo.Log;
import org.example.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
@MessageController
public class LogController {
    @Autowired
    LogService logService;
    @Operation(description = "查询函数，查询某一客户端的任务日志", summary = "查询函数")
    @GetMapping("/byname/{uname}")
    public List<Log> getByUname(@PathVariable(value = "uname", required = true) String uname){
        return logService.findByUname(uname);
    }
    @Operation(description = "查询函数，查询某一时间段的任务日志，time1<time2", summary = "查询函数")
    @GetMapping("/bytime")
    public List<Log> getByTime(@RequestParam(value = "time1", required = true)Timestamp time1,
                               @RequestParam(value = "time2", required = true)Timestamp time2) throws Exception {
        return logService.findByTime(time1, time2);
    }
    @Operation(description = "查询函数，查询某一客户端某一时间段的任务日志，time1<time2", summary = "查询函数")
    @GetMapping("/range/{uname}")
    public List<Log> getByTimeRangeAndUname(@RequestParam(value = "time1", required = true)Timestamp time1,
                                            @RequestParam(value = "time2", required = true)Timestamp time2,
                                            @PathVariable(value = "uname", required = true) String uname) throws Exception {
        return logService.findByTimeBetweenAndUname(time1, time2, uname);
    }
    @Operation(description = "查询函数，查询某一客户端特定时间的任务日志，只有一条", summary = "查询函数")
    @GetMapping("/detail/{uname}/{time}")
    public Log getDetail(@PathVariable(value = "uname", required = true) String uname,
                         @PathVariable(value = "time", required = true) Timestamp timestamp) throws Exception {
        return logService.findByTimeAndUname(timestamp, uname);
    }
    @Operation(description = "存储函数", summary = "存储函数")
    @PostMapping("/save")
    public void saveLog(@RequestBody Log log) throws Exception {
        logService.saveLog(log);
    }
    @Operation(description = "查询函数，查询所有日志", summary = "查询函数")
    @GetMapping("/")
    public List<Log> findAll(){
        return logService.findAll();
    }
}
