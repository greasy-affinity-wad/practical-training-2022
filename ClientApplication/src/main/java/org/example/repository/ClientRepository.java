package org.example.repository;

import org.example.pojo.Client;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
@Repository
public interface ClientRepository extends JpaRepository<Client, String>{
}
