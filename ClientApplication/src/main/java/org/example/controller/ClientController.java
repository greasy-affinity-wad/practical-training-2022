package org.example.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.example.annotation.MessageController;
import org.example.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.example.pojo.Client;

import java.util.List;

/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
//@RestController
@MessageController
@Tag(name = "客户端接口", description = "资源服务器，更新和提供客户端状态")
public class ClientController {
    @Autowired
    ClientService clientService;

    /**
     * @param id 要查询的mac地址/增加心跳的mac地址
     * @return 成功信息
     */
    @Operation(description = "用户端登入时，更新客户端状态/注册客户端，返回是否成功", summary = "登入函数")
    @GetMapping("/clientLogin")
    public void clientLogin(@RequestParam(name = "id", required = true) String id) throws Exception {

        clientService.clientLogin(id);
    }
    @Operation(description = "用户端登出时，更新客户端状态，返回是否成功", summary = "登出函数")
    @GetMapping("/clientLogout")
    public void clientLogout(@RequestParam(name = "id", required = true) String id) throws Exception {

        clientService.clientLogout(id);
    }

    /**
     * 用于服务器，查找所有
     * @return 成功信息
     */
    @Operation(description = "资源函数，使用以获取所有客户端信息", summary = "资源函数")
    @GetMapping("/getClients")
    public List<Client> getClients(){
        return clientService.getAll();
    }
}
