package org.example.pojo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


/**
 * @author 荆延龙
 * @version 1.0.0
 * data 2022/06/12
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "client")
public class Client {
    @Id
    private String id;
    //数据库只存时间戳
    @Column(name = "alive", nullable = false)
    private boolean alive;
}
