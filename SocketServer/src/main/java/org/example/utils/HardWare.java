package org.example.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HardWare {
    String info_os;
    String info_fullname;
    String info_os_architecture;
    String info_mu_languages;
    String info_SerialNumber;
    String info_cpu_count;
    String info_mainboard;
    String info_board_model;
    String info_systemtype;
    String info_physical_memory;
    String info_cpu_name;
    String info_clock_speed;
    String info_number_core;
    String info_data_width;
    String info_socket_desigination;
    String info_l2_cache;
    String info_l3_cache;
}
