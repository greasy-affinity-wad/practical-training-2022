package org.example.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Basic {
    String platform;
    String version;
    String architecture;
    String machine;
    String node;
    String processor;
    String system;
}
