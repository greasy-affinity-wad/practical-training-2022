package org.example.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SocketMessage {
    Log log;
    Basic basic;
    HardWare hardWare;
    Check[] check;
}
