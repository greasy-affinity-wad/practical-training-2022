package org.example.service;

import org.example.utils.Message;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Service
@FeignClient(value = "client")
public interface ClientService {
    @GetMapping("/clientLogin")
    public Message<Object> clientLogin(@RequestParam(name = "id", required = true) String id);
    @GetMapping("/clientLogout")
    public Message<Object> clientLogout(@RequestParam(name = "id", required = true) String id);
}
