package org.example.service;

import org.example.utils.Log;
import org.example.utils.Message;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@FeignClient(value = "log")
public interface LogService {
    @PostMapping("/save")
    public Message<Object> saveLog(@RequestBody Log log);
}
